﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;//establihses camera positioning in relation to the player object
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;//maintains establihsed camera position when player moves along x,z planes
    }
}
