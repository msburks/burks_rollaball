﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // sets all public and private variables
    public float speed = 0;
    public float sprintSpeed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private int count;
    private Rigidbody rb;
    private float movementX;
    private float movementY;

    public GameObject prefabToSpawn; // prefabToSpawn when needed

    // Start is called before the first frame update
    void Start()
        //establishes all necessary things at the start of the play
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winTextObject.SetActive(false);
    }
    // evaluates movement input and sets movement vectors, relaying into the fixedupdate function
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }


    // This function is displaying the count text on the screen using the built in unity UI
    void SetCountText()
    {
        countText.text = "Count:" + count.ToString();//what is displayed in UI for running count
        if (count >= 10)//sets threshold
        {
            winTextObject.SetActive(true);//displays win text
        }
    }

    // This function is updating the player movement based on movement
    // input from the above OnMove function
    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);//player can only move along x,z planes

        rb.AddForce(movement * speed);//add force to player object's rigidbody
    }

    //Update function updates speec based on spacebar press

    private void Update()
    {
        if (Keyboard.current.spaceKey.isPressed) //check is space is currently pressed
        {
            speed = sprintSpeed; //sets sprint speed in inspector
        }
        // reloads scene if player falls off of the level
        if (transform.position.y < -10f)// threshold for calling scenemanager stuff
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }


    // OnTriggerEnter function allows the player to interact with trigger colliders
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp")) //compares tag to see if matches str
        {
            other.gameObject.SetActive(false); //iff "pickup' tag, disable gameobject
            count = count + 1; //increase count
            SetCountText(); //call function

            //spawn new prefab pickup upon picking one up
            float randomRange = 5f; //range in which things will be randomly spawned

            //sets new random position, using randomRange position to set the ranges for the x,y,z position of the newly instantiated object
            Vector3 randomPosition = new Vector3(Random.Range(-randomRange, randomRange), Random.Range(0.0f, randomRange), Random.Range(-randomRange, randomRange));
            Instantiate(prefabToSpawn, randomPosition, prefabToSpawn.transform.rotation);
            // creates new object, using prefabToSpawn public variable, the new randomPosition, and the prefabs rotation
        }
        //Adding JumpPad upward force when trigger is entered
        if (other.gameObject.CompareTag("JumpPad"))
        {
            rb.AddForce(Vector3.up * 500f); //Add upward force
        }
    }
}
